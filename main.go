package main

import (
	"io"
	"log"
	"os"
)

func main() {
	data, err := io.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal("no stdin")
	}

	_, err = Replacer.WriteString(os.Stdout, string(data))
	if err != nil {
		log.Fatal("writing to stdout")
	}
}
